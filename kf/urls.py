
from django.conf.urls import url
from django.contrib import admin
from clinic import views
from django.views.generic import TemplateView

from clinic.views import RegisterForm,IndexView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', RegisterForm.as_view(template_name='index.html')),
    url(r'^', IndexView.as_view(template_name='index.html')),
]
