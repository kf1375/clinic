from django.forms import ModelForm
# from django.core.exceptions import ValidationError
from .models import registerForm


class registerForm(ModelForm):
    class Meta:
        model = registerForm
        fields = ['username', 'password', 'doctor_id', 'address', 'mobile',
                  'codeposti','first_name','last_name']
