# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core import validators

from django.shortcuts import render

from django.views import generic

from .forms import registerForm


# Create your views here.

class RegisterForm(generic.FormView):
    form_class = registerForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(RegisterForm, self).form_valid(form)

    def form_invalid(self, form):
        return super(RegisterForm, self).form_invalid(form)


#
class IndexView(generic.TemplateView):
    pass
