# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class registerForm(models.Model):
    username = models.CharField(max_length = 5000 , verbose_name='نام کاربری' )
    password = models.CharField(max_length = 5000 , verbose_name='رمز عبور' )
    # email = models.EmailField(max_length = 5000 , unique = True , verbose_name='ایمیل' )
    doctor_id = models.IntegerField(max_length=5000 , unique=True , verbose_name='شماره نظام پزشکی' , db_index=True  )
    address = models.CharField(max_length = 5000  , verbose_name='ادرس پستی' )
    mobile = models.IntegerField(max_length=500 , unique=True , verbose_name='تلفن همراه' )
    codeposti = models.IntegerField(max_length=5000, verbose_name='کد پستی' )
    first_name = models.CharField(max_length = 5000, verbose_name='نام' )
    last_name = models.CharField(max_length = 5000 , verbose_name='نام خانوادگی' )

    def __str__(self):
        # return f"{self.first_name}  {self.last_name}"
        return self.first_name

    class Meta:
        verbose_name = 'کاربر'
        verbose_name_plural = 'کاربران'
